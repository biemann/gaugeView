package biemann.gaugeview;


import java.util.ArrayList;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * GaugeView
 * <p>
 * Note: ScaleAngleStart - angle 0 is considered to be straight down
 * </p>
 * @author Alexander Biemann
 * @version 1.0
 */
public class GaugeView extends View
{
    // Constants
    final int MAXIMUM_HEIGHT = 500;// converted to DP later
    final float ANGLE_OFFSET = 90.0f;// 90degree offset so that 0degrees is downward instead of to the right
    final int TICK_GAUGE_SEPARATOR = 5;// converted to DP later
    final int NEEDLE_BASE_POSITION_CENTER = 0;// see attrs.xml:NeedleBasePosition
    final int NEEDLE_BASE_POSITION_TOPRIGHT = 1;
    final int NEEDLE_BASE_POSITION_TOPLEFT = 2;
    final int NEEDLE_BASE_POSITION_BOTTOM_LEFT = 3;
    final int NEEDLE_BASE_POSITION_BOTTOM_RIGHT = 4;
    final float DIAMETER_INCREASE_FACTOR = 0.8f;//80% (don't go over 1.0)

    // Member Variables
    private float mScreenDensity;
    private RectF mGaugeOuterArcRectangle;
    private RectF mGaugeMidArcRectangle;
    private RectF mGaugeInnerArcRectangle;
    private int mTickGaugeSeparator;
    private Paint mSegmentDividerPaint;
    private Paint mBackgroundPaint;
    private Paint mScaleUsedPaint;
    private Paint mScaleUnusedPaint;
    private Paint mScaleUsedEndcapPaint;//used to fix a bug with Android: the end-caps of the arc's are not square !
    private Paint mNeedlePaint;
    private Paint mTickPaint;
    private Paint mValueTextPaint;
    private Paint mUnitsTextPaint;
    private float mScaleAngleStartInternal;// = mScaleAngleStart + 90
    private float mScaleAngleEndInternal;// = mScaleAngleEnd + 90
    private float mScaleSweepUsed;
    private float mScaleSweepUnused;
    private PointF mNeedleStartCoordinates;
    private PointF mNeedleEndCoordinates;
    private double mAnglePerValue;
    private ArrayList<PointF> mTickCoordinatesMid;
    private ArrayList<PointF> mTickCoordinatesOuter;
    private PointF mValueTextCoordinates;
    private PointF mUnitsTextCoordinates;
    private PointF mFixedScaleEncapStart;
    private PointF mFixedScaleEncapEnd;


    // Attributes
    private int mBackgroundColor;
    private int mScaleUsedColor;
    private int mScaleUnusedColor;
    private int mNeedleColor;
    private int mTickColor;
    private int mValueTextColor;
    private int mUnitsTextColor;
    private int mScaleAngleStart;
    private int mScaleAngleEnd;
    private int mScaleWidth;
    private int mValueMin;
    private int mValueMax;
    private int mValue;
    private int mValueMajorPoint;
    private int mSegmentDividerWidth;
    private int mTickWidth;
    private int mTickLength;
    private int mNeedleWidth;
    private int mValueTextSize;
    private int mUnitsTextSize;
    private float mValueTextOffsetX;
    private float mUnitsTextOffsetX;
    private float mValueTextOffsetY;
    private float mUnitsTextOffsetY;
    private String mValueText;
    private String mUnitsText;
    private int mNeedleBasePosition;
    private boolean mHideNeedle;
    private boolean mHideValueText;
    private boolean mHideTicks;


    /**
     * Constructor for XML Inflation
     * @param context
     * @param attrs
     */
    public GaugeView(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        // Initialization
        mNeedleStartCoordinates = new PointF();
        mNeedleEndCoordinates = new PointF();
        mValueTextCoordinates = new PointF();
        mUnitsTextCoordinates = new PointF();
        mFixedScaleEncapStart = new PointF();
        mFixedScaleEncapEnd = new PointF();
        mScreenDensity = context.getResources().getDisplayMetrics().density;
        mTickGaugeSeparator = (int) (TICK_GAUGE_SEPARATOR * mScreenDensity);
        mGaugeOuterArcRectangle = new RectF();
        mGaugeMidArcRectangle = new RectF();
        mGaugeInnerArcRectangle = new RectF();
        mTickCoordinatesMid = new ArrayList<PointF>();
        mTickCoordinatesOuter = new ArrayList<PointF>();

        // Read all attributes
        readAttributes(attrs);

        // Paint Initialization
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setColor(mBackgroundColor);

        mScaleUsedPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mScaleUsedPaint.setStyle(Paint.Style.STROKE);
        mScaleUsedPaint.setStrokeWidth(mScaleWidth);
        mScaleUsedPaint.setColor(mScaleUsedColor);

        mScaleUnusedPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mScaleUnusedPaint.setStyle(Paint.Style.STROKE);
        mScaleUnusedPaint.setStrokeWidth(mScaleWidth);
        mScaleUnusedPaint.setColor(mScaleUnusedColor);

        mScaleUsedEndcapPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mScaleUsedEndcapPaint.setStyle(Paint.Style.STROKE);
        mScaleUsedEndcapPaint.setStrokeWidth(3f * mScreenDensity);//just a few DP wide - enough to cover the dark wedge at the end
        mScaleUsedEndcapPaint.setColor(mScaleUsedColor);

        mNeedlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mNeedlePaint.setStyle(Paint.Style.STROKE);
        mNeedlePaint.setStrokeWidth(mNeedleWidth);
        mNeedlePaint.setColor(mNeedleColor);

        mTickPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTickPaint.setStyle(Paint.Style.STROKE);
        mTickPaint.setStrokeWidth(mTickWidth);
        mTickPaint.setColor(mTickColor);

        mValueTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mValueTextPaint.setColor(mValueTextColor);
        mValueTextPaint.setTextSize(mValueTextSize);
        mValueTextPaint.setTextAlign(Align.CENTER);

        mUnitsTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mUnitsTextPaint.setColor(mUnitsTextColor);
        mUnitsTextPaint.setTextSize(mUnitsTextSize);
        mUnitsTextPaint.setTextAlign(Align.CENTER);

        mSegmentDividerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mSegmentDividerPaint.setStyle(Paint.Style.STROKE);
        mSegmentDividerPaint.setStrokeWidth(mSegmentDividerWidth);
        mSegmentDividerPaint.setColor(mBackgroundColor);


        // Calculations
        mAnglePerValue = (mScaleAngleEnd - mScaleAngleStart) / (double)(mValueMax - mValueMin);
    }

    /**
     * Process all the custom attributes
     * @param attrs
     */
    private void readAttributes(AttributeSet attrs)
    {
        TypedArray attrsArray = getContext().obtainStyledAttributes(attrs, R.styleable.GaugeView);

        // Read the Attribute values from XML. Uses default values for unspecified attributes
        mBackgroundColor = attrsArray.getInteger(R.styleable.GaugeView_BackgroundColor, Color.BLACK);
        mScaleUsedColor = attrsArray.getInteger(R.styleable.GaugeView_ScaleUsedColor, Color.DKGRAY);
        mScaleUnusedColor = attrsArray.getInteger(R.styleable.GaugeView_ScaleUnusedColor, Color.WHITE);
        mNeedleColor = attrsArray.getInteger(R.styleable.GaugeView_NeedleColor, Color.GRAY);
        mTickColor = attrsArray.getInteger(R.styleable.GaugeView_TickColor, Color.WHITE);
        mValueTextColor = attrsArray.getInteger(R.styleable.GaugeView_ValueTextColor, Color.WHITE);
        mUnitsTextColor = attrsArray.getInteger(R.styleable.GaugeView_UnitsTextColor, Color.WHITE);
        mScaleAngleStart = attrsArray.getInteger(R.styleable.GaugeView_ScaleAngleStart, 30);
        mScaleAngleStartInternal = mScaleAngleStart + ANGLE_OFFSET;//redefines the 0 angle to be straight down
        mScaleAngleEnd = attrsArray.getInteger(R.styleable.GaugeView_ScaleAngleEnd, 330);
        mScaleAngleEndInternal = mScaleAngleEnd + ANGLE_OFFSET;//redefines the 360 angle to be straight down
        mScaleWidth = (int) (attrsArray.getDimension(R.styleable.GaugeView_ScaleWidth, 50 * mScreenDensity + 0.5f));
        mValueMin = attrsArray.getInteger(R.styleable.GaugeView_ValueMin, 0);
        mValueMax = attrsArray.getInteger(R.styleable.GaugeView_ValueMax, 100);
        mValue = attrsArray.getInteger(R.styleable.GaugeView_Value, 20);
        mValueMajorPoint = attrsArray.getInteger(R.styleable.GaugeView_ValueMajorPoint, 10);
        mSegmentDividerWidth = (int) (attrsArray.getDimension(R.styleable.GaugeView_SegmentDividerWidth, 3 * mScreenDensity + 0.5f));
        mTickWidth = (int) (attrsArray.getDimension(R.styleable.GaugeView_TickWidth, 5 * mScreenDensity + 0.5f));
        mTickLength = (int) (attrsArray.getDimension(R.styleable.GaugeView_TickLength, 9 * mScreenDensity + 0.5f));
        mNeedleWidth = (int) (attrsArray.getDimension(R.styleable.GaugeView_NeedleWidth, 7 * mScreenDensity + 0.5f));
        mValueTextSize = (int) attrsArray.getDimension(R.styleable.GaugeView_ValueTextSize, 20 * mScreenDensity + 0.5f);
        mUnitsTextSize = (int) attrsArray.getDimension(R.styleable.GaugeView_UnitsTextSize, 20 * mScreenDensity + 0.5f);
        mUnitsText = attrsArray.getString(R.styleable.GaugeView_UnitsText);
        mNeedleBasePosition = attrsArray.getInteger(R.styleable.GaugeView_NeedleBasePosition, NEEDLE_BASE_POSITION_CENTER);
        mHideNeedle = attrsArray.getBoolean(R.styleable.GaugeView_HideNeedle, false);
        mHideValueText = attrsArray.getBoolean(R.styleable.GaugeView_HideValueText, false);
        mHideTicks = attrsArray.getBoolean(R.styleable.GaugeView_HideTicks, false);
        mValueTextOffsetX = attrsArray.getFloat(R.styleable.GaugeView_ValueTextOffsetX, 50f);//0..100%
        mUnitsTextOffsetX = attrsArray.getFloat(R.styleable.GaugeView_UnitsTextOffsetX, 50f);//0..100%
        mValueTextOffsetY = attrsArray.getFloat(R.styleable.GaugeView_ValueTextOffsetY, 60f);//0..100%
        mUnitsTextOffsetY = attrsArray.getFloat(R.styleable.GaugeView_UnitsTextOffsetY, 61f);//0..100%

        // API requirement to call recycle.
        attrsArray.recycle();

        //validate important values to ensure that they're within limits
        if (mValueTextOffsetX > 100f) mValueTextOffsetX=100f;
        if (mValueTextOffsetX < 0f) mValueTextOffsetX=0f;
        if (mUnitsTextOffsetX > 100f) mUnitsTextOffsetX=100f;
        if (mUnitsTextOffsetX < 0f) mUnitsTextOffsetX=0f;
        if (mValueTextOffsetY > 100f) mValueTextOffsetY=100f;
        if (mValueTextOffsetY < 0f) mValueTextOffsetY=0f;
        if (mUnitsTextOffsetY > 100f) mUnitsTextOffsetY=100f;
        if (mUnitsTextOffsetY < 0f) mUnitsTextOffsetY=0f;
    }


    /**
     * Helper Method for onMeasure.
     * Take MeasureSpec into account and return a size
     */
    private int decideWidth(int mode, int size)
    {
        if (mode == MeasureSpec.AT_MOST || mode == MeasureSpec.EXACTLY)
        {
            return size;
        }
        else
        {
            // for MeasureSpec.UNSPECIFIED
            return getMaximumWidth();
        }
    }


    /**
     * Helper Method for onMeasure.
     * Take MeasureSpec into account and return a size
     * @return size when mode is AT_MOST or EXACTLY, otherwise some large value
     */
    private int decideHeight(int mode, int size)
    {
        if (mode == MeasureSpec.AT_MOST || mode == MeasureSpec.EXACTLY)
        {
            return size;
        }
        else
        {
            // for MeasureSpec.UNSPECIFIED
            return getMaximumHeight();
        }
    }


    /**
     * Helper Method for onMeasure.
     * @return getMaximumHeight() as width since this is a square view
     */
    private int getMaximumWidth()
    {
        return getMaximumHeight();
    }


    /**
     * Helper Method for onMeasure.
     * @return maximum height of this view
     */
    private int getMaximumHeight()
    {
        // Convert to DP
        int height = (int)(MAXIMUM_HEIGHT * mScreenDensity);

        return height;
    }


    /**
     * Android View callback
     * Called a few times whilst the views are being laid-out
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        int widthAllowed = MeasureSpec.getSize(widthMeasureSpec);
        int heightAllowed = MeasureSpec.getSize(heightMeasureSpec);

        widthAllowed = decideWidth(widthMode, widthAllowed);
        heightAllowed = decideHeight(heightMode, heightAllowed);

        final int squareWidthAndHeight = Math.min(widthAllowed, heightAllowed);

        setMeasuredDimension(squareWidthAndHeight, squareWidthAndHeight);
    }

    /**
     * Android View callback
     * Called once onMeasure() is finished with
     */
    @Override
    protected void onSizeChanged(int drawableWidth, int drawableHeight, int oldw, int oldh)
    {
        super.onSizeChanged(drawableWidth, drawableHeight, oldw, oldh);

        // if the needle base is not at center of Gauge, then rescale the diameter to 80% of original and reposition needle base
        final float enlargeDiameterPercent = (mNeedleBasePosition == NEEDLE_BASE_POSITION_CENTER)? 1.0f : 1.0f+DIAMETER_INCREASE_FACTOR;

        // reduce diameter to account for outer ticks and Scale Width
        final int prescaledMinDiameter = Math.min(drawableWidth, drawableHeight);
        final int intendedOuterGaugeDiameter = (int) (prescaledMinDiameter * enlargeDiameterPercent);
        final int intendedMidGaugeDiameter = intendedOuterGaugeDiameter - (2 * mTickLength);
        final int maxIntendedInnerGaugeDiameter = intendedMidGaugeDiameter - (2 * mTickGaugeSeparator);
        final int halfMaxIntendedInnerGaugeDiameter = maxIntendedInnerGaugeDiameter / 2;
        final int tenPercentOfQuarterMaxIntendedInnerGaugeDiameter = (int) (0.15f * halfMaxIntendedInnerGaugeDiameter);
        final int MaxScaleWidth = halfMaxIntendedInnerGaugeDiameter - tenPercentOfQuarterMaxIntendedInnerGaugeDiameter;
        if (mScaleWidth > MaxScaleWidth)
        {
            mScaleWidth = MaxScaleWidth;

            mScaleUsedPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mScaleUsedPaint.setStyle(Paint.Style.STROKE);
            mScaleUsedPaint.setStrokeWidth(mScaleWidth);
            mScaleUsedPaint.setColor(mScaleUsedColor);

            mScaleUnusedPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mScaleUnusedPaint.setStyle(Paint.Style.STROKE);
            mScaleUnusedPaint.setStrokeWidth(mScaleWidth);
            mScaleUnusedPaint.setColor(mScaleUnusedColor);
        }
        final int intendedInnerGaugeDiameter = intendedMidGaugeDiameter - (2 * mTickGaugeSeparator) - mScaleWidth;
        //note about line above: mScaleWidth is not multiplied by 2 because Android draws the arc from the center of a thick line


        // new square rectangles
        // the inner rectangle will contain the Gauge, the ticks will be drawn between the mid and outer
        mGaugeOuterArcRectangle.set(0, 0, intendedOuterGaugeDiameter, intendedOuterGaugeDiameter);
        mGaugeMidArcRectangle.set(0, 0, intendedMidGaugeDiameter, intendedMidGaugeDiameter);
        mGaugeInnerArcRectangle.set(0, 0, intendedInnerGaugeDiameter, intendedInnerGaugeDiameter);
        mGaugeMidArcRectangle.offsetTo(mTickLength, mTickLength);
        mGaugeInnerArcRectangle.offsetTo(mTickLength + mTickGaugeSeparator + mScaleWidth/2,
                mTickLength + mTickGaugeSeparator + mScaleWidth/2);
        //note about line above: mScaleWidth is divided by 2 because Android draws the arc from the center of a thick line

        // reposition the centers of the rectangles depending on the chosen position (mNeedleBasePosition)
        // NOTE: depending on DIAMETER_INCREASE_FACTOR, the center of the page will shift around
        final float middleOfView = 2+(1-DIAMETER_INCREASE_FACTOR);
        float offsetX = 0f;
        float offsetY = 0f;
        switch(mNeedleBasePosition)
        {
            case NEEDLE_BASE_POSITION_CENTER:
                //nothing needs to be done
                break;
            case NEEDLE_BASE_POSITION_TOPRIGHT:
                offsetY = -(intendedOuterGaugeDiameter/middleOfView);
                break;
            case NEEDLE_BASE_POSITION_TOPLEFT:
                offsetX = -(intendedOuterGaugeDiameter/middleOfView);
                offsetY = -(intendedOuterGaugeDiameter/middleOfView);
                break;
            case NEEDLE_BASE_POSITION_BOTTOM_LEFT:
                offsetX = -(intendedOuterGaugeDiameter/middleOfView);
                break;
            case NEEDLE_BASE_POSITION_BOTTOM_RIGHT:
                //nothing needs to be done
                break;
        }
        mGaugeOuterArcRectangle.offset(offsetX, offsetY);
        mGaugeMidArcRectangle.offset(offsetX, offsetY);
        mGaugeInnerArcRectangle.offset(offsetX, offsetY);

        // Recalculate center of view for start of needle line
        float needleStartX = mGaugeInnerArcRectangle.width()/2f + mGaugeInnerArcRectangle.left;
        float needleStartY = mGaugeInnerArcRectangle.height()/2f + mGaugeInnerArcRectangle.top;
        mNeedleStartCoordinates.set(needleStartX, needleStartY);

        recalculateCoordinates();
    }

    /**
     * Helper function used to calculate coordinates when values are set
     */
    private void recalculateCoordinates()
    {
        // Recalculate the angle on the scale that represents the Value point
        // knowing this angle allows calculation of the sweeps
        float angle = (int) ((mValue * mAnglePerValue) + mScaleAngleStartInternal);
        mScaleSweepUsed = (float) (angle - mScaleAngleStartInternal + 0.5f);//the 0.5f prevents and annoying graphical glitch that's present in Android
        //NOTE: a negative value for mScaleSweepUnused is desirable, see onDraw() ripple note
        mScaleSweepUnused = (float) (angle - mScaleAngleEndInternal + 0.5f);//the 0.5f prevents and annoying graphical glitch that's present in Android


        // Recalculate the end point of the needle line
        //double arcRadius = (mGaugeInnerArcRectangle.width() + mScaleWidth) / 2.0f;//This makes the needle end at the outside of the scale
        double arcRadius = mGaugeOuterArcRectangle.width() / 2.0f;//This makes the needle end at the outside of the ticks
        double radangle = Math.toRadians(angle);
        double cosangle = Math.cos(radangle) * arcRadius;
        double sinangle = Math.sin(radangle) * arcRadius;
        float xCoordinate = (float) (cosangle + mNeedleStartCoordinates.x );
        float yCoordinate = (float) (sinangle + mNeedleStartCoordinates.y );
        mNeedleEndCoordinates.set(xCoordinate, yCoordinate);


        // Calculate the tick positions and put them in arrays
        if (mValueMajorPoint > 0)
        {
            mTickCoordinatesMid.clear();
            mTickCoordinatesOuter.clear();
            for (int value = mValueMin; value <= mValueMax; value += mValueMajorPoint )
            {
                angle = (float) ((value * mAnglePerValue) + mScaleAngleStartInternal);
                arcRadius = mGaugeMidArcRectangle.width() / 2.0f;
                radangle = Math.toRadians(angle);
                cosangle = Math.cos(radangle) * arcRadius;
                sinangle = Math.sin(radangle) * arcRadius;
                xCoordinate = (float) (cosangle + mNeedleStartCoordinates.x );
                yCoordinate = (float) (sinangle + mNeedleStartCoordinates.y );
                mTickCoordinatesMid.add(new PointF(xCoordinate, yCoordinate));
            }
            for (int value = mValueMin; value <= mValueMax; value += mValueMajorPoint )
            {
                angle = (float) ((value * mAnglePerValue) + mScaleAngleStartInternal);
                arcRadius = mGaugeOuterArcRectangle.width() / 2.0f;
                radangle = Math.toRadians(angle);
                cosangle = Math.cos(radangle) * arcRadius;
                sinangle = Math.sin(radangle) * arcRadius;
                xCoordinate = (float) (cosangle + mNeedleStartCoordinates.x );
                yCoordinate = (float) (sinangle + mNeedleStartCoordinates.y );
                mTickCoordinatesOuter.add(new PointF(xCoordinate, yCoordinate));
            }
        }
        // Note: To save on calculations, the mTickCoordinatesMid[] coordinates are used for the end-points of the segment dividers.

        // Calculate coordinates to put Value text
        if (mHideValueText == false)
        {
            xCoordinate = ((mValueTextOffsetX / 100f) * mGaugeOuterArcRectangle.width()) + mGaugeOuterArcRectangle.left;
            yCoordinate = ((mValueTextOffsetY / 100f) * mGaugeOuterArcRectangle.height()) + mGaugeOuterArcRectangle.top;
            mValueTextCoordinates.set(xCoordinate, yCoordinate);
            mValueText = String.valueOf(mValue);
        }

        // Calculate coordinates to put Units text
        xCoordinate = ((mUnitsTextOffsetX / 100f) * mGaugeOuterArcRectangle.width()) + mGaugeOuterArcRectangle.left;
        yCoordinate = ((mUnitsTextOffsetY / 100f) * mGaugeOuterArcRectangle.height()) + mGaugeOuterArcRectangle.top;
        mUnitsTextCoordinates.set(xCoordinate, yCoordinate);

        // fix Android's endcap problem with stroke'd paint
        // NOTE: this problem is only visible if a needle is hidden. If you don't hide the needle then feel free to comment this out as well as point 1.5 in onDraw()
        if ((mHideNeedle == true) && (mValue > mValueMin) && (mValue < mValueMax))
        {
            angle = mScaleAngleStartInternal+mScaleSweepUsed;
            radangle = Math.toRadians(angle);
            arcRadius = (mGaugeInnerArcRectangle.width() / 2.0f)+(mScaleWidth/2)+0.5f;//without the 0.5 the line isn't flush with the rest of the arc
            cosangle = Math.cos(radangle) * arcRadius;
            sinangle = Math.sin(radangle) * arcRadius;
            xCoordinate = (float) (cosangle + mNeedleStartCoordinates.x );
            yCoordinate = (float) (sinangle + mNeedleStartCoordinates.y );
            mFixedScaleEncapStart.set(xCoordinate, yCoordinate);

            radangle = Math.toRadians(mScaleAngleStartInternal+mScaleSweepUsed+180+0.5f);//opposite direction and angle the line a little bit inward
            arcRadius = mScaleWidth;
            cosangle = Math.cos(radangle) * arcRadius;
            sinangle = Math.sin(radangle) * arcRadius;
            xCoordinate = (float) (cosangle + mFixedScaleEncapStart.x );
            yCoordinate = (float) (sinangle + mFixedScaleEncapStart.y );
            mFixedScaleEncapEnd.set(xCoordinate, yCoordinate);
        }
    }

    /**
     * Override onDraw() to customize what the View shows.
     * This View  retains the call to the superclass to inherit basic View functionality
     * Called once onSizeChanged() is done with
     * <p>
     * Note: This method is called *often* so it *must* be efficient; avoid calculations whenever possible
     * </p>
     */
    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        // 0. Draw Background
        canvas.drawRect(mGaugeOuterArcRectangle, mBackgroundPaint);

        // 1. Draw Scale
        //first, draw the arc indicating the values used
        canvas.drawArc(mGaugeInnerArcRectangle, mScaleAngleStartInternal, mScaleSweepUsed, false, mScaleUsedPaint);
        //second, draw the arc indicating the values "unused"
        //NOTE: always start drawing the arc at the bottom and draw towards the value. Otherwise a visible ripple can be observed
        canvas.drawArc(mGaugeInnerArcRectangle, mScaleAngleEndInternal, mScaleSweepUnused, false, mScaleUnusedPaint);

        // 1.5 - fix Android's endcap problem with stroke'd paint which becomes visible if a needle is hidden
        //       if you comment this code out and hide the needle you'll see a small gap on the outer side, where the two scale sections meet
        if ((mHideNeedle == true) && (mValue > mValueMin) && (mValue < mValueMax))
        {
            canvas.drawLine(mFixedScaleEncapStart.x, mFixedScaleEncapStart.y, mFixedScaleEncapEnd.x, mFixedScaleEncapEnd.y, mScaleUsedEndcapPaint);
        }

        // 2. Draw Scale Segments (separators) same color as background
        if ((mSegmentDividerWidth > 0) && (mValueMajorPoint > 0))
        {
            final int iMaxSegmentIndex = mTickCoordinatesMid.size();
            for (int index=0; index<iMaxSegmentIndex; index++)
            {
                final PointF endPoint = mTickCoordinatesMid.get(index);
                canvas.drawLine(mNeedleStartCoordinates.x, mNeedleStartCoordinates.y, endPoint.x, endPoint.y, mSegmentDividerPaint);
            }
        }

        // 3. Draw Outer Ticks
        if ((mValueMajorPoint > 0) && (mHideTicks == false))
        {
            final int iMaxTickIndex = mTickCoordinatesMid.size();
            for (int index=0; index<iMaxTickIndex; index++)
            {
                final PointF startPoint = mTickCoordinatesMid.get(index);
                final PointF endPoint = mTickCoordinatesOuter.get(index);
                canvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y, mTickPaint);
            }
        }

        // 4. Draw Needle
        if (mHideNeedle == false)
        {
            canvas.drawLine(mNeedleStartCoordinates.x, mNeedleStartCoordinates.y,
                    mNeedleEndCoordinates.x,   mNeedleEndCoordinates.y,
                    mNeedlePaint);
        }

        // 5. Draw Value as text
        if (mValueText != null)
        {
            canvas.drawText(mValueText, mValueTextCoordinates.x, mValueTextCoordinates.y, mValueTextPaint);
        }

        // 6. Draw Units (label)
        if (mUnitsText != null)
        {
            canvas.drawText(mUnitsText, mUnitsTextCoordinates.x, mUnitsTextCoordinates.y, mUnitsTextPaint);
        }

    }

    public void setValue(int value)
    {
        mValue = value;
        recalculateCoordinates();
        invalidate(); // Trigger onDraw()
    }

}
