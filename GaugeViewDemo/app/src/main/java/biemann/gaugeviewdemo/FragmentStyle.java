package biemann.gaugeviewdemo;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import biemann.gaugeview.GaugeView;

public class FragmentStyle extends Fragment
{
    // member variables
    private SeekBar mSeekBar;
    private ArrayList<GaugeView> mGaugeList;

    // constructor
    public FragmentStyle()
    {}

    // fragment lifecycle callback
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_style, container, false);

        mGaugeList = new ArrayList<GaugeView>();
        mGaugeList.add((GaugeView) rootView.findViewById(R.id.gaugeView1));
        mGaugeList.add((GaugeView) rootView.findViewById(R.id.gaugeView2));
        mGaugeList.add((GaugeView) rootView.findViewById(R.id.gaugeView3));
        mGaugeList.add((GaugeView) rootView.findViewById(R.id.gaugeView4));

        mSeekBar = (SeekBar) rootView.findViewById(R.id.seekBar1);
        mSeekBar.setOnSeekBarChangeListener(new SeekBarChangeListener());
        mSeekBar.requestFocus();

        return rootView;
    }

    // seekbar callbacks
    private class SeekBarChangeListener implements OnSeekBarChangeListener
    {

        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {
            // Do Nothing
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {
            // Do nothing
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
        {
            // update gauges
            setValueAllGauges(progress);
        }
    }

    //set values on all gauges in this view simultaneously
    private void setValueAllGauges(int progress)
    {
        for (GaugeView view : mGaugeList)
        {
            view.setValue(progress);
        }
    }
}