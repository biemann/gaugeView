# gaugeView
Gauge View library and Test Project for Android.
Used to display gauges and circular progress bars


## Documentation about using the library##
https://gitlab.com/biemann/gaugeView/blob/master/Documents/GaugeView.pdf


## Library Source ##
https://gitlab.com/biemann/gaugeView/tree/master/GaugeViewDemo/gaugeview


## Test Project Source ##
https://gitlab.com/biemann/gaugeView/tree/master/GaugeViewDemo/app


## Demo app on the Play Store ##
https://play.google.com/store/apps/details?id=biemann.gaugeviewdemo


## Screenshots ##
![](https://gitlab.com/biemann/gaugeView/raw/master/Documents/pic1_circular.jpg)

![](https://gitlab.com/biemann/gaugeView/raw/master/Documents/pic2_right_angled.jpg)

![](https://gitlab.com/biemann/gaugeView/raw/master/Documents/pic3_styled.jpg)

![](https://gitlab.com/biemann/gaugeView/raw/master/Documents/pic4_loaders.jpg)
